Project PCO IHM morfose : 

Compile and Run the Project with maven and javafx:run :

```bash
mvn clean javafx:run
```

Create a custom runtime image : 

To create a custom runtime, run from a terminal with JAVA_HOME set: 
```bash
mvn clean javafx:jlink
```
To clean répo use : 
```
mvn clean
```
using java with jar obselete version : 

import lib :
```shell
sudo tar xf javafx-sdk-18.0.1.tar.xz -C /opt
sudo ln -s opt/javafx-sdk-18.0.1 /opt/javafx
sudo nano etc/profile.d/javafx.sh 
```
in file etc/profile.d/javafx.sh : 
```script
export JAVA_HOME=/usr/lib/jvm/default-java
export JAVA_FX=/opt/javafx
export JAVA_FX_HOME=/opt/javafx
```
```shell
sudo chmod +x /etc/profile.d/javafx.sh
source /etc/profile.d/javafx.sh
```
Compile with javac : 
```bash 
javac --module-path $JAVA_FX --add-modules javafx.controls,javafx.fxml App.java
```
Run with : 
```bash
java --module-path $JAVA_FX --add-modules javafx.controls App
```